<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Like;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WelcomeController extends Controller
{
    public function index(){

        $posts = Post::orderBy('created_at','DESC')->paginate(5);
//            dd($posts);
            return view('welcome',compact('posts'));
//        }

    }
}
