<?php

namespace App\Http\Controllers;

use App\Jobs\SendDislikeJob;
use App\Jobs\SendlikeJob;
use App\Like;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{

    private $paths;

    public function __construct()
    {
        $this->middleware('auth');
        $this->paths = public_path('/image/posts');
    }

    public function store()
    {

        try {
            $this->validate(request(), [
                'tittle' => 'required',
                'message' => 'required|max:350',
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        } catch (ValidationException $e) {
            dd($e->getMessage());
            return redirect()->back()->withErrors($e->errors())->with('error', $e->getMessage());
        }
//        dd(\request()->all());
        if (request()->hasFile('photo')) {

            $photo = request()->file('photo');

            if (!is_dir($this->paths)) {
                mkdir($this->paths, 0777);
            }
            $name = sha1(date('YmdHis') . str_random(30));
            $save_name = $name . '.' . $photo->getClientOriginalExtension();

            // this creates and saves the thumbnail image
            Image::make($photo)
                ->resize(250, null, function ($constraints) {
                    $constraints->aspectRatio();
                });
            $photo->move($this->paths, $save_name);

            $post = new Post();
            $post->tittle = request()->tittle;
            $post->message = request()->message;
            $post->user_id = Auth::user()->id;
            $post->image = $save_name;
            $post->save();
        }
            // this saves the actual image



            return back()->with('status', Auth::user()->name . ' ' . 'Your Post added successfully');
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
//    public function like(Post $post)
//
//    {
////        dd(Auth::user());
//        Like::updateOrCreate([
//            //Add unique field combo to match here
//            //For example, perhaps you only want one entry per user:
//            'user_id'   => Auth::user()->id,
//            'post_id'   => $post,
//        ],[
//            'like'     => false,
//
//        ]);
//
////        SendlikeJob::dispatch($post->id,Auth::user()->id);
//
//            return back()->with('status', Auth::user()->name.' '."You like This Post");
//        }
//    public function dislike(Post $post)
//
//    {
//        Like::updateOrCreate([
//            //Add unique field combo to match here
//            //For example, perhaps you only want one entry per user:
//            'user_id'   => Auth::user()->id,
//            'post_id'   => $post,
//        ],[
//            'like'     => false,
//
//        ]);
//
////       SendDislikeJob::dispatch($post->id,Auth::user()->id);
//
//            return back()->with('status', Auth::user()->name.' '."You Dislike This Post");
//        }
}







