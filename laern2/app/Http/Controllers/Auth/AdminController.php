<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin',['except' => ['logout']]);
    }

    public function loginForm(){

        return view('auth.adminLogin');
    }

    public function login(){

        try {
            $this->validate(request(), [

                'email' => 'required|email',
                'password' => 'required'
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()->withErrors($e->errors())->withInput(request()->only('email'));
        }
//        dd(\request()->all());
        if(auth()->guard('admin')->attempt(['email'=>request()->email, 'password' => request()->password],request()->remember)){

            return redirect()->intended(route('admin.index'));
        }

        return redirect()->back()->withInput(request()->only('email','remember'))->withErrors(['email'=>'These credentials do not match our records']);
    }


    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }
}
