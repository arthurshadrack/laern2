<?php

namespace App\Http\Controllers;

use App\Like;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{


    public function like(Post $post)

    {
//        dd(Auth::user());
        Like::updateOrCreate([
            //Add unique field combo to match here
            //For example, perhaps you only want one entry per user:
            'user_id'   => Auth::user()->id,
            'post_id'   => $post->id,
        ],[
            'like'     => true,

        ]);

//        SendlikeJob::dispatch($post->id,Auth::user()->id);

        return back()->with('status', Auth::user()->name.' '."You like This Post");
    }
    public function dislike(Post $post)

    {
        Like::updateOrCreate([
            //Add unique field combo to match here
            //For example, perhaps you only want one entry per user:
            'user_id'   => Auth::user()->id,
            'post_id'   => $post->id,
        ],[
            'like'     => false,

        ]);

//       SendDislikeJob::dispatch($post->id,Auth::user()->id);

        return back()->with('status', Auth::user()->name.' '."You Dislike This Post");
    }

}
