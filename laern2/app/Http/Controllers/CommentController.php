<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(){

        try {
            $this->validate(request(), [

                'comment' => 'required',
                'post_id' => 'required',

            ]);
        } catch (ValidationException $e) {
            return redirect()->back()->withErrors($e->errors())->with('error',$e->getMessage());
        }
//        dd(\request()->all());
        $comment = new Comment();
        $comment->comment = request()->comment ;
        $comment->post_id = request()->post_id ;
        $comment->user_id = Auth::user()->id;
        $comment->save();

        return back()->with('status', Auth::user()->name.' '.'You Comment On THis Post');
    }

}
