<?php

namespace App\Http\Controllers;

use App\Post;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    private $paths;


    public function __construct()
    {
        $this->middleware('auth');
        $this->paths =public_path('/image/users');
    }

    public function update()
    {

        try {
            $this->validate(request(), [
                'name' => 'required',
                'password' => 'required',
                'email' => 'required',
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()->withErrors($e->errors())->with('error', $e->getMessage());
        }

        $save_name = auth()->user()->profile->photo;
        if (request()->hasFile('photo')) {

            $photo = request()->file('photo');

            if (!is_dir($this->paths)) {
                mkdir($this->paths, 0777);
            }
            $name = sha1(date('YmdHis') . str_random(30));
            $old_file = $save_name;
            $save_name = $name . '.' . $photo->getClientOriginalExtension();

            // this creates and saves the thumbnail image
            Image::make($photo)
                ->resize(250, null, function ($constraints) {
                    $constraints->aspectRatio();
                });

            // this saves the actual image
            $photo->move($this->paths, $save_name);

            if (File::exists("images/users/$old_file") && $old_file != '/avatar5.png') {
                unlink("images/users/$old_file");
            }
//            dd(request()->all());
            $user = User::find(Auth::User()->id);
            $user->name = request()->name;
            $user->email = request()->email;
            $user->password = Hash::make(request()->password);
            $user->save();

            $profile = Profile::find(auth()->user()->profile->id);
            $profile->user_id = auth()->user()->id;
            $profile->photo = $save_name;
//        dd(\request());
            $profile->push();

        }
        return back()->with('status', 'Post added successfully');

    }






}
