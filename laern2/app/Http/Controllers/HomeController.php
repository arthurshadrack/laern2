<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
        public function index()
    {
        $posts = Post::where('user_id',auth()->user()->id)->orderBy('created_at','DESC')->paginate(5);
    //      dd($posts);
        $userPostCount = Post::where('user_id',Auth::User()->id)->get()->count();

        return view('home',compact('posts','userPostCount'));
    }


}
