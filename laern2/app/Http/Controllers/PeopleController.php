<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PeopleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function people(){
        $peoples = User::orderBy('name','ASC')->get();
        return view('peoples',compact('peoples'));
    }


    public function profile()
    {
        $profile = new Profile();
        $profile->photo = 'avatar5.png';
        $profile->user_id = Auth::user()->id;
        $profile->save();

    }
}
