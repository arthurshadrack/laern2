<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){

        return view('admin_home');
    }


    public function show(){

        return view('auth.adminRegister');
    }

    public function store(){

        try {
            $this->validate(request(), [
                'email' => 'required|email|unique:admins,email',
                'password' => 'required|min:6',
            ]);
        } catch (ValidationException $e) {
//            dd($e->getMessage());
            return redirect()->back()->withErrors($e->errors())->withInput(request()->all())->with('error',$e->getMessage())->with('error', $e->getMessage());
        }
//
//        dd(request()->all());
        $admin = new Admin();
        $admin->email = request()->email;
        $admin->password = Hash::make(request()->password);
        $admin->save();
        return back()->with('status','User created');
    }

}
