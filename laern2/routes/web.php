<?php

use App\Post;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','WelcomeController@index')->name('all.post');

Auth::routes();

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('user.logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/post', 'PostController@store')->name('posts.store');

Route::post('/comment', 'CommentController@add')->name('comments.store');

Route::post('/posts/{post}', 'LikeController@like')->name('posts.like');

Route::post('/dislike/{post}', 'LikeController@dislike')->name('posts.dislike');

Route::post('/profile/update', 'ProfileController@update')->name('profile.update');




Route::prefix('people')->group(function (){

    Route::get('','PeopleController@people')->name('users.show');

    Route::post('','PeopleController@profile')->name('users.profile');

});



Route::prefix('admin')->group(function (){

    Route::get('', 'Auth\AdminController@loginForm');

    Route::post('/user/login', 'Auth\AdminController@login')->name('admin.login');

    Route::get('/show', 'Admin\AdminController@show')->name('admin.register.show');

    Route::post('/store', 'Admin\AdminController@store')->name('admin.store');

    Route::get('/index', 'Admin\AdminController@index')->name('admin.index');

    Route::get('/logout', 'Auth\AdminController@logout')->name('admin.logout');

});
