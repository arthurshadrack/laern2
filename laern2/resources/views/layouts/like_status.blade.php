@if (session('status'))
    <div class="alert alert-success bg-success alert-dismissible fade show flash-message text-white" id ="flash-message" role="alert" style="max-width: 50%;position: relative;text-align: left">
        {{ session('status') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
