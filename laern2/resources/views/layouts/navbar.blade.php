<nav class="navbar navbar-expand-md navbar-light bg-dark shadow-sm">
    <div class="container">
        <a class="navbar-brand text-white" href="{{ url('/') }}">
            Home
        </a>
        <a class="navbar-brand text-white" href="{{ route('users.show') }}">
            People
        </a>
        <a class="navbar-brand text-white" href="{{ url('/') }}">
            About-Us
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authen ication Links -->
                <ul class="navbar-nav mr-auto">
                    <form class="form-inline my-2 my-lg-0" id="search_form">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </ul>
                @guest
                    <li class="nav-item">
                        <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown shadow-lg">
                        <a id="navbar Dropdown" class="nav-link text-white ml-4 dropdown-toggle " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                           <span class="caret shadow-lg bg-white text-dark p-1"> {{ucfirst(Auth::user()->name ) }} </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right bg-warning justify-content-end" aria-labelledby="navbar Dropdown">
                            <h6 class="text-center bg-white shadow-sm pt-2">Profile Info</h6>
                            <a class="dropdown-item text-white text-center"  data-toggle="modal" data-target="#profile-update-{{Auth::User()->id}}" data-whatever="@getbootstrap">Update Profile</a>
                            <a class="dropdown-item text-white text-center" href="{{ route('user.logout') }}">Logout</a>
                            <form id="logout-form" action="#" method="get" style="display: none;">
                                @csrf
                            </form>

                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>