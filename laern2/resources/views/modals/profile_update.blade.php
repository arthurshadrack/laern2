<div class="modal fade justify-content-md-center" id="profile-update-{{Auth::User()->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <h5 class="modal-title " id="exampleModalLabel"><span class="text-center text-warning ml-3 shadow-sm p-1">
                       Your Profile
                    </span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ">
                <div class="card mb-2 mt-2">
                    <div style="width: 15rem; margin-top: 15px">
                        <div style="margin-left:55px">
                            @if(\App\Profile::where('user_id',Auth::user()->id)->exists())
                                <img src="/image/users/{{Auth::User()->profile->photo}}" class="w-75 img-fluid img-thumbnail img-circle img-fluid rounded-circle">
                                <figure class="-medium text-white mt-2">{{ucfirst(Auth::User()->name)}}</figure>
                            @else
                                <img src="/image/users/avatar5.png" class="w-75 img-fluid img-thumbnail img-circle img-fluid rounded-circle">
                                <figure class="-medium text-white mt-2">{{ucfirst(Auth::User()->name)}}</figure>
                            @endif
                     </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('profile.update')}}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="name" class="col-form-label col-md-12">Name</label>
                                <input type="text" id="photo" name="name"  value="{{Auth::User()->name}}"
                                       class="form-control {{$errors->has('name') ? ' is-invalid' : ''}}" autocomplete="on">
                                @if($errors->has('name'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-form-label col-md-12">Password</label>
                                <input type="email" id="email" name="email"  value=""
                                       class="form-control {{$errors->has('email') ? ' is-invalid' : ''}}" autocomplete="off">
                                @if($errors->has('email'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="photo" class="col-form-label col-md-12">Password</label>
                                <input type="password" id="photo" name="password"
                                       class="form-control {{$errors->has('password') ? ' is-invalid' : ''}}" autocomplete="off">
                                @if($errors->has('password'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="photo" class="col-form-label col-md-12">Your Image</label>
                                <input type="file" id="photo" name="photo">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-warning my-2 my-sm-0 bg-dark text-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-outline-warning my-2 my-sm-0 bg-dark text-white">Update</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>



















