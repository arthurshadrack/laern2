<div class="modal fade  justify-content-md-center" id="view-user-posts-{{Auth::User()->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop-limit="1" aria-hidden="true" data-modal-parent="#profile-update-{{Auth::User()->id}}">
    <div class="modal-content">
        <div class="modal-header justify-content-center">
            <h5 class="modal-title " id="exampleModalLabel"><span class="text-center text-warning ml-3 shadow-sm p-1">
                       Your Profile
                    </span></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body ">
            <div class="card mb-2">
                <div class="card mr-5" style="width: 18rem;">
                    <img src="/image/users/avatar5.png" class="card-img-top img-circle img-fluid rounded-circle" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
                <div class="card-body">

                    <form action="{{route('comments.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="photo" class="col-form-label col-md-12"></label>
                            <input type="file" id="photo" name="photo">
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-form-label col-md-12"></label>
                            <input type="text" id="photo" name="name"  value="{{ old('name') }}"
                                   class="form-control {{$errors->has('name') ? ' is-invalid' : ''}}" autocomplete="off">
                            @if($errors->has('name'))
                                <span class="invalid-feedback text-danger" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="photo" class="col-form-label col-md-12"></label>
                            <input type="password" id="photo" name="name"  value="{{ old('name') }}"
                                   class="form-control {{$errors->has('name') ? ' is-invalid' : ''}}" autocomplete="off">
                            @if($errors->has('name'))
                                <span class="invalid-feedback text-danger" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="hidden" name="post_id" value="">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-warning my-2 my-sm-0 bg-dark text-white" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline-warning my-2 my-sm-0 bg-dark text-white">Send message</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>



















