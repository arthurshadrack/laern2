<div class="modal fade" id="post-comment-{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <h5 class="modal-title " id="exampleModalLabel"><span class="text-center text-warning ml-3 shadow-sm p-1">
                        Posted By {{$post->user->name.' '.\Carbon\Carbon::parse($post->user->created_at)->format('d-M-Y')}}
                    </span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card mb-2">
                    <h6 class="ml-3 "><span class="btn btn-outline-dark mt-2">Post Tittle</span></h6>
                    <h5 class="card-header font-weight-bold ">{{$post->tittle}}</h5>
                    <div class="card-body">
                        <h5 class="card-title"><span class="btn btn-outline-dark mt-0">Post Body</span></h5>
                        <p class="card-text" style="text-transform: capitalize">{{$post->message}}</p>
                        <form action="{{route('comments.store')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="comment" class="col-form-label">Your Comments:</label>
                                <textarea class="form-control" id="comment" name="comment" required></textarea>
                            </div>
                            <input type="hidden" name="post_id" value="{{$post->id}}">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-warning my-2 my-sm-0 bg-dark text-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-outline-warning my-2 my-sm-0 bg-dark text-white">Send message</button>
                            </div>
                        </form>
                    </div>
                    </div>
           </div>
    </div>
</div>



















