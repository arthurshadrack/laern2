<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Laravel</title>
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link rel="stylesheet" href="css/font-awesome/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            /*height: 100vh;*/
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }
        figure{
            border-right: 20px solid #007bff!important;
            border-bottom-right-radius: 200px;
            border-top-right-radius: 200px
        }

        /*.links > a {*/
        /*color: #636b6f;*/
        /*padding: 0 25px;*/
        /*font-size: 13px;*/
        /*font-weight: 600;*/
        /*letter-spacing: .1rem;*/
        /*text-decoration: none;*/
        /*text-transform: uppercase;*/
        /*}*/

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
@include('layouts.navbar')
<div class="container border-pink-4">
    <div class=" ">
        <div class="card mb-5 mt-3 bg-primary shadow-lg">
            <div class="row no-gutters">
                <div class="col-md-2">
                    {{--<img src="/image/gun.png" class="card-img" alt="...">--}}
                    <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner ml-2 mt-2">
                            <div class="carousel-item active" data-interval="10000">
                                <img src="/image/gun.png" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item" data-interval="2000">
                                <img src="/image/gun.png" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/image/gun.png" class="d-block w-100" alt="...">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-8 ">
                    <div class="card-body">
                        <a href="{{route('home')}}" class="card-title btn btn-outline-dark ">Create A Post</a>
                        <p class="card-text text-white">This is a wider card with supporting text below as a natural The Bootstrap and Vue scaffolding provided by
                            Laravel is located in the laravel/ui Composer package, which may be installed using Composer: lead-in to additional content. This content is a little bit longer.</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>

                    </div>
                </div>
                <div class="col-md-2 ">
                    <div class="card-body border-left">
                        @guest
                            <img src="/image/users/{{Auth::User()->profile->photo}} "  class="w-100 img-fluid img-thumbnail rounded-circle">
                            <figure class="-medium text-white mt-2">{{ucfirst(Auth::User()->name)}}</figure>
                            <h5 class="card-title btn btn-outline-dark ">My Posts <span class="text-white pl-1 pr-1" style="border: 2px solid white; border-radius: 5px">{{count(Auth::User()->posts)}}</span></h5>

                        @else
                            <div class="span" style="display: none">nothing to show</div>
                             @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <span class="flex-center">@include('layouts.like_status')</span>
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-3" >
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-header">Events</div>
                        <div class="card-body">
                            <ul class="list-group list-group-flush ">
                                <li class="list-group-item text-primary">Cras justo odio</li>
                                <li class="list-group-item text-primary">Dapibus ac facilisis in</li>
                                <li class="list-group-item text-primary">Morbi leo risus</li>
                                <li class="list-group-item text-primary">Porta ac consectetur ac</li>
                                <li class="list-group-item text-primary">Vestibulum at eros</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mr-xl-4 ml-xl-5" >
                    @foreach($peoples as $people)
                        @php

                        @endphp
                        <div class="card mb-2">
                            <div class="card mb-3" style="max-width: 540px;">
                                <div class="row no-gutters">
                                    <div class="col-md-4">
                                        @if(\App\Profile::where('user_id',$people->id)->exists())
                                            <img src="/image/users/{{$people->profile->photo}}" class="w-100 img-fluid img-thumbnail img-circle img-fluid rounded-circle">
                                        @else
                                            <img src="/image/users/avatar5.png" class="w-100 img-fluid img-thumbnail img-circle img-fluid rounded-circle">
                                        @endif
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <p class="card-text"><span class="font-weight-bold">Name:</span> {{$people->name.' Join  on  '.\Carbon\Carbon::parse($people->created_at)->format('d-M-Y')}}</p>
                                            <p class="card-text"><span class="font-weight-bold">Email:</span> {{$people->email}}</p>
                                            <p class="card-text"><small class="text-muted"><span class="font-weight-bold">Phone #</span> 025346785492</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex m-3">
                                <svg viewBox="0 0 20 20">
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="icon-shape">
                                            <form>
                                                @csrf
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <button type="submit" class="btn btn-warning">Fellow</button>
                                                </div>
                                            </form>
                                            <form action="" method="post">
                                                @csrf
                                                <div class="btn-group ml-1" role="group" aria-label="Basic example">
                                                <button type="submit" class="btn btn-warning">Message</button>
                                                </div>
                                            </form>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="col-md-2" >
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-header">Events</div>
                        <div class="card-body">
                            <ul class="list-group list-group-flush ">
                                <li class="list-group-item text-primary">Cras justo odio</li>
                                <li class="list-group-item text-primary">Dapibus ac facilisis in</li>
                                <li class="list-group-item text-primary">Morbi leo risus</li>
                                <li class="list-group-item text-primary">Porta ac consectetur ac</li>
                                <li class="list-group-item text-primary">Vestibulum at eros</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<div class="container ">
    <div class="col-md-12 justify-content-center">
        {{--{{$posts->render()}}--}}
    </div>
</div>
<script>
    $('.carousel').carousel()
    jQuery(document).on("click", "button.closeChild", function (e) {
        jQuery('.childModel').modal('hide');
    });

    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    });

    // $("document").ready(function(){
    //     setTimeout(function(){
    //         $(".alert").remove();
    //     }, 2500 ); // 5 secs
    //
    // });
</script>
</body>
</html>
