@extends('layouts.app')
@section('content')
    <div class="container border-pink-4">
        <div class="row">
            <div class="card mb-5 mt-3 bg-primary shadow-lg">
                <div class="row no-gutters">
                    <div class="col-md-2">
                        <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner ml-2 mt-2">
                                <div class="carousel-item active" data-interval="10000">
                                    <img src="/image/gun.png" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item" data-interval="2000">
                                    <img src="/image/gun.png" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="/image/gun.png" class="d-block w-100" alt="...">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 ">
                        <div class="card-body">
                            <h5 class="card-title btn btn-outline-dark ">Update Your Profile</h5>
                            <p class="card-text text-white">This is a wider card with supporting text below as a natural The Bootstrap and Vue scaffolding provided by
                                Laravel is located in the laravel/ui Composer package, which may be installed using Composer: lead-in to additional content. This content is a little bit longer.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                    <div class="col-md-2 ">
                        <div class="card-body border-left">
                       ADMIN
                            <figure class="-medium text-white mt-2">{{auth()->user('auth:admin')->email}}</figure>
                            <a href="{{route('admin.logout')}}" class="-medium text-white mt-2  border-right">Logout</a>
                            <a href="{{route('admin.register.show')}}" class="-medium text-white mt-2">register</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-8" >
                        <form action="{{route('posts.store')}}" method="post" id="post_form">
                            @csrf
                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1" >Post Tittle</label>
                                <input type="text" name="tittle" {{$errors->has('tittle') ? ' is-invalid' : ''}}  value="{{old('tittle')}}" class="form-control" id="exampleFormControlInput1" placeholder="Tittle Here" required>
                                @if ($errors->has('tittle'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $errors->first('tittle') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-12">
                                <label for="message">Post Title</label>
                                <textarea class="form-control" name="message" id="message" {{$errors->has('message') ? ' is-invalid' : ''}}  value="{{old('message')}}"  rows="5" placeholder="Body Here" required></textarea>
                                @if ($errors->has('message'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-outline-primary my-2 my-sm-0 bg-dark" form="post_form">Send</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4" >
                        <div class="row justify-content-center mt-4">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header">{{ __('Dashboard') }}</div>
                                    <div class="card-body">

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        $('.carousel').carousel()
    </script>
@endsection
